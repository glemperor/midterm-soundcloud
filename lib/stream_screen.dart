import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class StreamScreen extends StatelessWidget {
  const StreamScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        elevation: 0,
          automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        actions: []),
      body: ListView(
        
        children: [
          
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                
                Text(
                  'Stream',
                 style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 28,
                    color: Colors.white),
                  ),
                  SizedBox(height: 30,),

                  Row(
                    children: [
                      CircleAvatar(
                          backgroundColor: Color(0xFF801b1f)
                      ),
                      SizedBox(width: 10,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('SLOWED AND REVERB',
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.italic,
                                color: Colors.white),),
                          SizedBox(
                            height: 3,
                          ),
                          Text('posted a playlist · 3 days ago',
                            style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey)  )
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image(
                      image: NetworkImage('https://i1.sndcdn.com/artworks-9YF24YTbGM8MmDHu-yJpzAQ-t500x500.jpg',
                          ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Icon(Icons.favorite_outline,color: Colors.grey,),
                      SizedBox(width: 5,),
                      Text('39',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      SizedBox(width: 20,),
                      Icon(Icons.repeat_outlined,color: Colors.grey,),
                      Text('13',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      SizedBox(
                        width: 250,
                      ),
                      Container(child: Icon(Icons.more_vert,color: Colors.grey,))
                    ],
                  ),
                  SizedBox(height: 20,),
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: Color(0xFF801b1f)
                      ),
                      SizedBox(width: 10,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('SLOWED AND REVERB',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.italic,
                            color: Colors.white),),
                          SizedBox(
                            height: 3,
                          ),
                          Text('posted a playlist · 4 days ago',
                            style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey)  )
                        ],
                      )
                    ],
                  ),

                SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image(
                    image: NetworkImage('https://i1.sndcdn.com/artworks-vOe8zyHzoEtaFZCh-JTBFuQ-t500x500.jpg',
                    ),
                  ),
                ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Icon(Icons.favorite_outline,color: Colors.grey,),
                      SizedBox(width: 5,),
                      Text('47',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      SizedBox(width: 20,),
                      Icon(Icons.repeat_outlined,color: Colors.grey,),
                      Text('28',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      SizedBox(
                        width: 250,
                      ),
                      Container(child: Icon(Icons.more_vert,color: Colors.grey,))
                    ],
                  ),

                //1
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    CircleAvatar(
                        backgroundImage: NetworkImage("https://i1.sndcdn.com/avatars-BITW1y4m50kVITM3-uON53w-t50x50.jpg"),
                    ),
                    SizedBox(width: 10,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('denvau',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              // fontStyle: FontStyle.italic,
                              color: Colors.white),),
                        SizedBox(
                          height: 3,
                        ),
                        Text('posted a playlist · 3 days ago',
                            style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w400,
                                color: Colors.grey)  )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image(
                    image: NetworkImage('https://i1.sndcdn.com/artworks-kesjwzHlB4tzOTbG-NAsPtg-t500x500.jpg',
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Icon(Icons.favorite_outline,color: Colors.grey,),
                    SizedBox(width: 5,),
                    Text('21',style: TextStyle(color: Colors.grey,fontSize: 15),),
                    SizedBox(width: 20,),
                    Icon(Icons.repeat_outlined,color: Colors.grey,),
                    Text('90',style: TextStyle(color: Colors.grey,fontSize: 15),),
                    SizedBox(
                      width: 250,
                    ),
                    Container(child: Icon(Icons.more_vert,color: Colors.grey,))
                  ],
                ),

                //2
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    CircleAvatar(
                        backgroundColor: Color(0xFF801b1f)
                    ),
                    SizedBox(width: 10,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('SLOWED AND REVERB',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.italic,
                              color: Colors.white),),
                        SizedBox(
                          height: 3,
                        ),
                        Text('posted a playlist · 3 days ago',
                            style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w400,
                                color: Colors.grey)  )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image(
                    image: NetworkImage('https://i1.sndcdn.com/artworks-4TTc8Y3N2z6H6KP0-muajxA-t500x500.jpg',
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Icon(Icons.favorite_outline,color: Colors.grey,),
                    SizedBox(width: 5,),
                    Text('99',style: TextStyle(color: Colors.grey,fontSize: 15),),
                    SizedBox(width: 20,),
                    Icon(Icons.repeat_outlined,color: Colors.grey,),
                    Text('16',style: TextStyle(color: Colors.grey,fontSize: 15),),
                    SizedBox(
                      width: 250,
                    ),
                    Container(child: Icon(Icons.more_vert,color: Colors.grey,))
                  ],
                ),

                //3
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    CircleAvatar(
                        backgroundColor: Color(0xFF801b1f)
                    ),
                    SizedBox(width: 10,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('SLOWED AND REVERB',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.italic,
                              color: Colors.white),),
                        SizedBox(
                          height: 3,
                        ),
                        Text('posted a playlist · 9 days ago',
                            style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w400,
                                color: Colors.grey)  )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image(
                    image: NetworkImage('https://i1.sndcdn.com/artworks-r3OICOxS2Ny6bVL0-bej5Zg-t500x500.jpg',
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Icon(Icons.favorite_outline,color: Colors.grey,),
                    SizedBox(width: 5,),
                    Text('20',style: TextStyle(color: Colors.grey,fontSize: 15),),
                    SizedBox(width: 20,),
                    Icon(Icons.repeat_outlined,color: Colors.grey,),
                    Text('10',style: TextStyle(color: Colors.grey,fontSize: 15),),
                    SizedBox(
                      width: 250,
                    ),
                    Container(child: Icon(Icons.more_vert,color: Colors.grey,))
                  ],
                ),


              ],
            ),
          )
        ],
      ),
    );
  }
}
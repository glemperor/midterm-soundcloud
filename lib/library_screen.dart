import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mysoundcloud/account_info.dart';
import 'package:mysoundcloud/detail_screen.dart';

class LibraryScreen extends StatefulWidget {
  const LibraryScreen({Key? key}) : super(key: key);

  @override
  State<LibraryScreen> createState() => _LibraryScreenState();
}

class _LibraryScreenState extends State<LibraryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          elevation: 0,
          actions: [
            Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const AccountInfo()),
                    );
                  },
                  child: CircleAvatar(
                    backgroundImage: NetworkImage('https://lh3.googleusercontent.com/a-/AOh14GgRk0YcMRytT_omkonSktONQd1VsSV0XVQ00Otr6w=s83-c-mo'),
              ),
                ),

            )
          ]),
      body: ListView(
        children: [
          Container(
            color: Colors.black,
            height: 1200,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Library',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 28, color: Colors.white),),

                  Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: SizedBox(
                      height: 15,),
                  ),
                   Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Liked tracks',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),

                         Icon(
                                CupertinoIcons.chevron_right,
                              color: Colors.white,
                              size: 15,
                            ),
                      ],
                    ),
                  SizedBox(
                    height: 28,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Playlists',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      ),

                      Icon(
                        CupertinoIcons.chevron_right,
                        color: Colors.white,
                        size: 15,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 28,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Albums',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      ),

                      Icon(
                        CupertinoIcons.chevron_right,
                        color: Colors.white,
                        size: 15,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 28,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Following',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      ),

                      Icon(
                        CupertinoIcons.chevron_right,
                        color: Colors.white,
                        size: 15,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 28,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Stations',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      ),

                      Icon(
                        CupertinoIcons.chevron_right,
                        color: Colors.white,
                        size: 15,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [ Text('Recently Played',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 28
                      ),),
                      Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Text('See All',
                          style: TextStyle(
                            fontSize: 13,
                            color: Color(0xFF999999),
                          ),),
                        ],
                      )
                  ]
                  ),
                  SizedBox(height: 15,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: <Widget>[
                        Column(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-zVtkBDuMQkNGhcYB-AK49cg-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Cho anh một chút hy vọng',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Hau Kong',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-CS2gSPnCyzqqH7vi-EIHOuQ-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Hông về tình iu',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Khoi Vu',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-000677383411-qib59o-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Berlin',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Khoi Vu',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-GTyuryEUgfi7xJUy-93K8UA-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('mot nguoi vi em',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('WEAN',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-2hmbHOTnVSnyOICu-suNy3A-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("DON'T BREAK MY HEART",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('BINZ x Touliver',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        // SizedBox(
                        //   width: 15.0,
                        // ),
                        // Column(
                        //   children: [
                        //     InkWell(
                        //       onDoubleTap: (){
                        //         Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailScreen()));
                        //       },
                        //       child: Container(
                        //         height: MediaQuery.of(context).size.height * 0.2,
                        //         width: MediaQuery.of(context).size.width * 0.3,
                        //         child: Image(image: NetworkImage('https://file.vfo.vn/hinh/2018/03/hinh-anh-hinh-nen-noo-phuoc-thinh-dep-nhat-de-thuong-2.jpg'),
                        //
                        //
                        //         ),
                        //       ),
                        //     ),
                        //
                        //     Text('Khong con cua nhau'),
                        //     SizedBox(height: 5,),
                        //     Text('Ten tac gia')
                        //
                        //   ],
                        // ),
                        // SizedBox(
                        //   width: 10.0,
                        // ),

                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [ Text('Listening history',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 28
                        ),),
                        Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Text('See All',
                              style: TextStyle(
                                fontSize: 13,
                                color: Color(0xFF999999),
                              ),),
                          ],
                        )
                      ]
                  ),
                  SizedBox(height: 15,),
                  SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 80,
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Row(
                                  children: [
                                    Container(
                                      width: 70,
                                      height: 70,
                                      child: ClipRRect(
                                              borderRadius: BorderRadius.circular(5.0),
                                              child: Image(
                                                image: NetworkImage('https://i1.sndcdn.com/artworks-zVtkBDuMQkNGhcYB-AK49cg-t500x500.jpg',),
                                                  fit: BoxFit.cover,)
                                          ),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),

                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                            SizedBox(
                                              height: 13,
                                            ),
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text('Cho anh một chút hy vọng',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w500
                                                ),),
                                            ),
                                            SizedBox(height: 5,),
                                            Text('Hau Kong',
                                                style: TextStyle(
                                                    color: Color(0xFF999999),
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w500
                                                ),),
                                            SizedBox(height: 5,),
                                            Row(
                                              children: [
                                                Icon(
                                                  CupertinoIcons.play_arrow_solid,
                                                  size: 14,
                                                  color: Color(0xFF999999),
                                                ),
                                                Text('198K · 2:41',
                                                  style: TextStyle(
                                                      color: Color(0xFF999999),
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w500
                                                  ),),
                                              ],
                                            )
                                          ],
                                        ),



                                  ]
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 18.0,
                        ),
                        Row(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 80,
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Row(
                                  children: [
                                    Container(
                                      width: 70,
                                      height: 70,
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(5.0),
                                          child: Image(
                                            image: NetworkImage('https://i1.sndcdn.com/artworks-P1IAjGD9StXDGZ58-kKlBzA-t500x500.jpg',),
                                            fit: BoxFit.cover,)
                                      ),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),

                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 13,
                                        ),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text('Lavie - WEAN (reup)',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500
                                            ),),
                                        ),
                                        SizedBox(height: 5,),
                                        Text('H N G',
                                          style: TextStyle(
                                              color: Color(0xFF999999),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500
                                          ),),
                                        SizedBox(height: 5,),
                                        Row(
                                          children: [
                                            Icon(
                                              CupertinoIcons.play_arrow_solid,
                                              size: 14,
                                              color: Color(0xFF999999),
                                            ),
                                            Text('128K · 2:49',
                                              style: TextStyle(
                                                  color: Color(0xFF999999),
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500
                                              ),),
                                          ],
                                        )
                                      ],
                                    ),



                                  ]
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 18.0,
                        ),
                        Row(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 80,
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Row(
                                  children: [
                                    Container(
                                      width: 70,
                                      height: 70,
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(5.0),
                                          child: Image(
                                            image: NetworkImage('https://i1.sndcdn.com/artworks-000570554138-4r1m2m-t200x200.jpg',),
                                            fit: BoxFit.cover,)
                                      ),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),

                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 13,
                                        ),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text('Phiêu Bồng',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500
                                            ),),
                                        ),
                                        SizedBox(height: 5,),
                                        Text('Tofu',
                                          style: TextStyle(
                                              color: Color(0xFF999999),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500
                                          ),),
                                        SizedBox(height: 5,),
                                        Row(
                                          children: [
                                            Icon(
                                              CupertinoIcons.play_arrow_solid,
                                              size: 14,
                                              color: Color(0xFF999999),
                                            ),
                                            Text('298K · 3:41',
                                              style: TextStyle(
                                                  color: Color(0xFF999999),
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500
                                              ),),
                                          ],
                                        )
                                      ],
                                    ),



                                  ]
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 18.0,
                        ),
                        Row(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 80,
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Row(
                                  children: [
                                    Container(
                                      width: 70,
                                      height: 70,
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(5.0),
                                          child: Image(
                                            image: NetworkImage('https://i1.sndcdn.com/artworks-VLkrdtDJQszwzVVQ-VmcU3Q-t200x200.jpg',),
                                            fit: BoxFit.cover,)
                                      ),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),

                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 13,
                                        ),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text('Loving You Sunny',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500
                                            ),),
                                        ),
                                        SizedBox(height: 5,),
                                        Text('Tlinh',
                                          style: TextStyle(
                                              color: Color(0xFF999999),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500
                                          ),),
                                        SizedBox(height: 5,),
                                        Row(
                                          children: [
                                            Icon(
                                              CupertinoIcons.play_arrow_solid,
                                              size: 14,
                                              color: Color(0xFF999999),
                                            ),
                                            Text('18K · 2:56',
                                              style: TextStyle(
                                                  color: Color(0xFF999999),
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500
                                              ),),
                                          ],
                                        )
                                      ],
                                    ),



                                  ]
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 18.0,
                        ),
                        Row(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 80,
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Row(
                                  children: [
                                    Container(
                                      width: 70,
                                      height: 70,
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(5.0),
                                          child: Image(
                                            image: NetworkImage('https://i1.sndcdn.com/artworks-N7NW3FHXW8CnGcjJ-eeqcCA-t200x200.jpg',),
                                            fit: BoxFit.cover,)
                                      ),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),

                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 13,
                                        ),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text('All For You',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500
                                            ),),
                                        ),
                                        SizedBox(height: 5,),
                                        Text('the sg kid',
                                          style: TextStyle(
                                              color: Color(0xFF999999),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500
                                          ),),
                                        SizedBox(height: 5,),
                                        Row(
                                          children: [
                                            Icon(
                                              CupertinoIcons.play_arrow_solid,
                                              size: 14,
                                              color: Color(0xFF999999),
                                            ),
                                            Text('198K · 2:41',
                                              style: TextStyle(
                                                  color: Color(0xFF999999),
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500
                                              ),),
                                          ],
                                        )
                                      ],
                                    ),



                                  ]
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 18.0,
                        ),
                        // SizedBox(
                        //   width: 15.0,
                        // ),
                        // Column(
                        //   children: [
                        //     InkWell(
                        //       onDoubleTap: (){
                        //         Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailScreen()));
                        //       },
                        //       child: Container(
                        //         height: MediaQuery.of(context).size.height * 0.2,
                        //         width: MediaQuery.of(context).size.width * 0.3,
                        //         child: Image(image: NetworkImage('https://file.vfo.vn/hinh/2018/03/hinh-anh-hinh-nen-noo-phuoc-thinh-dep-nhat-de-thuong-2.jpg'),
                        //
                        //
                        //         ),
                        //       ),
                        //     ),
                        //
                        //     Text('Khong con cua nhau'),
                        //     SizedBox(height: 5,),
                        //     Text('Ten tac gia')
                        //
                        //   ],
                        // ),
                        // SizedBox(
                        //   width: 10.0,
                        // ),

                      ],
                    ),
                  ),
                ]
              )
            )
          )
  ]

            ),

    );



  }
}
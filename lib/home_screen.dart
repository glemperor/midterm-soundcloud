import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mysoundcloud/detail_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String imageUrl = 'https://image.thanhnien.vn/w660/Uploaded/2022/znetns/2021_03_26/725a8593_kmyk.jpg';
  bool _loadImageError = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          elevation: 0,
          actions: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 25, 10),
              child: Icon(
                CupertinoIcons.arrow_up_circle,
                size: 30,
                color: Color(0xFF989898),
              ),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
                child: Icon(
                  CupertinoIcons.bell,
                  size: 30,
                  color: Color(0xFF989898),
                )

            )
          ]),
      body: ListView(
        children: [
          Container(
            color: Colors.black,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Home',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 28, color: Colors.white),),
                  Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: SizedBox(
                      height: 15,),
                  ),
                  Text(
                    'Latest tracks from people you follow',
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Column(
                        children: [CircleAvatar(
                          backgroundColor: Colors.deepOrange,
                          radius: 30.2,
                          child: CircleAvatar(
                            backgroundImage: NetworkImage('https://i1.sndcdn.com/avatars-BITW1y4m50kVITM3-uON53w-t50x50.jpg'),
                            radius: 28.0,
                          ),
                        ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "denvau",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                              fontSize: 11,
                            ),
                          )
                      ]),
                    SizedBox(
                      width: 15,
                    ),
                      Column(
                          children: [CircleAvatar(
                            backgroundColor: Colors.deepOrange,
                            radius: 30.2,
                            child: CircleAvatar(
                              backgroundColor: Color(0xFF801b1f),
                              radius: 28.0,
                            ),
                          ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "SLOWED A...",
                              style: TextStyle(
                                fontWeight: FontWeight.w900,
                                color: Colors.white,
                                fontSize: 11,
                                fontStyle: FontStyle.italic
                              ),
                            )
                          ]),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text('Picks For You',
                    style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                      'Since you liked nhạc Việt Nam & Young Music VN, we think you will like',
                    style: TextStyle(
                        fontSize: 15,
                        color: Color(0xFF989898),
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(5.0),
                      child: Image.network('https://image.thanhnien.vn/w660/Uploaded/2022/znetns/2021_03_26/725a8593_kmyk.jpg')),

                  SizedBox(height: 5,),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: CircleAvatar(
                          backgroundImage: this._loadImageError?null:NetworkImage(this.imageUrl),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Text('    Inspired by nhạc Việt Nam,Young Music VN and more', style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF989898),
                           ),),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text('Recently Played',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 28
                    ),),
                  SizedBox(height: 15,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: <Widget>[
                        Column(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                children: [
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(5.0),
                                      child: Image(
                                      image: NetworkImage('https://i1.sndcdn.com/artworks-zVtkBDuMQkNGhcYB-AK49cg-t500x500.jpg'),)
                                  ),
                                  SizedBox(height: 10,),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text('Cho anh một chút hy vọng',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500
                                      ),),
                                  ),
                                  SizedBox(height: 5  ,),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text('Hau Kong',
                                      style: TextStyle(
                                          color: Color(0xFF999999),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500
                                      ),),
                                  )
                              ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-CS2gSPnCyzqqH7vi-EIHOuQ-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Hông về tình iu',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Khoi Vu',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-000677383411-qib59o-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Berlin',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Khoi Vu',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-GTyuryEUgfi7xJUy-93K8UA-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('mot nguoi vi em',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('WEAN',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-2hmbHOTnVSnyOICu-suNy3A-t500x500.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("DON'T BREAK MY HEART",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('BINZ x Touliver',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        // SizedBox(
                        //   width: 15.0,
                        // ),
                        // Column(
                        //   children: [
                        //     InkWell(
                        //       onDoubleTap: (){
                        //         Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailScreen()));
                        //       },
                        //       child: Container(
                        //         height: MediaQuery.of(context).size.height * 0.2,
                        //         width: MediaQuery.of(context).size.width * 0.3,
                        //         child: Image(image: NetworkImage('https://file.vfo.vn/hinh/2018/03/hinh-anh-hinh-nen-noo-phuoc-thinh-dep-nhat-de-thuong-2.jpg'),
                        //
                        //
                        //         ),
                        //       ),
                        //     ),
                        //
                        //     Text('Khong con cua nhau'),
                        //     SizedBox(height: 5,),
                        //     Text('Ten tac gia')
                        //
                        //   ],
                        // ),
                        // SizedBox(
                        //   width: 10.0,
                        // ),

                      ],
                    ),
                  ),
                  // SizedBox(
                  //   height: 10,
                  // ),
                  SizedBox(height: 10,),
                  Text('Charts: New & hot',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 28
                    ),
                  ),
                  SizedBox(height: 8,),
                  Text(
                    'Up-and-coming tracks on SoundCloud',
                    style: TextStyle(
                        fontSize: 15,
                        color: Color(0xFF989898),
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(height: 10,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: <Widget>[
                        Column(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-kesjwzHlB4tzOTbG-NAsPtg-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('All music genres',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('New & hot',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-qA57OeG3twYNmWsz-usBPrw-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Hip-hop & Rap',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('New & hot',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-buRwac7D0XKO7rog-jcz6yw-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Pop',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('New & hot',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-ulEaJOBfEPLIc7jF-ILa9cQ-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('House',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('New & hot',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/artworks-JrsE9OAgA6yCMZiR-PcZ9IQ-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("RnB & Soul",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Hot & new',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                 
                  Text('Artists You Should Know',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 28
                    ),
                  ),
                  SizedBox(height: 8,),
                  Text(
                    'Top tracks from artists similar to Khoi Vu',
                    style: TextStyle(
                        fontSize: 15,
                        color: Color(0xFF989898),
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(height: 10,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: <Widget>[
                        Column(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-D1nDYRK1rfqje9DH-T1eqng-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('fishy',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Top Tracks',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-jssxfQBVrG1qRxl3-q552gA-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('bánh mì pơ tỏi',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Top Tracks',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-qbKTNcAR2B4Hxh5A-iCx6jQ-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Đặng Gia Linh',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Top Tracks',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-qRlzphJtprKDAUib-2uDGng-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Hawys.',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Top Tracks',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(5.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-B4KwswX95RSOk9AF-DRZAmA-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Gii",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text('Top Tracks',
                                        style: TextStyle(
                                            color: Color(0xFF999999),
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        // SizedBox(
                        //   width: 15.0,
                        // ),
                        // Column(
                        //   children: [
                        //     InkWell(
                        //       onDoubleTap: (){
                        //         Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailScreen()));
                        //       },
                        //       child: Container(
                        //         height: MediaQuery.of(context).size.height * 0.2,
                        //         width: MediaQuery.of(context).size.width * 0.3,
                        //         child: Image(image: NetworkImage('https://file.vfo.vn/hinh/2018/03/hinh-anh-hinh-nen-noo-phuoc-thinh-dep-nhat-de-thuong-2.jpg'),
                        //
                        //
                        //         ),
                        //       ),
                        //     ),
                        //
                        //     Text('Khong con cua nhau'),
                        //     SizedBox(height: 5,),
                        //     Text('Ten tac gia')
                        //
                        //   ],
                        // ),
                        // SizedBox(
                        //   width: 10.0,
                        // ),

                      ],
                    ),
                  ),

                  // Text('Artists You Should Know',
                  //   style: TextStyle(
                  //       fontWeight: FontWeight.bold,
                  //       color: Colors.white,
                  //       fontSize: 28
                  //   ),
                  // ),
                  // SizedBox(height: 8,),
                  // Text(
                  //   'Top tracks from artists similar to Khoi Vu',
                  //   style: TextStyle(
                  //       fontSize: 15,
                  //       color: Color(0xFF989898),
                  //       fontWeight: FontWeight.w500),
                  // ),
                  // SizedBox(height: 10,),
                  // SingleChildScrollView(
                  //   scrollDirection: Axis.horizontal,
                  //   child: Row(
                  //     children: <Widget>[
                  //       Column(
                  //         children: [
                  //           Container(
                  //             // color: Colors.red,
                  //             height: 200,
                  //             width: MediaQuery.of(context).size.width * 0.3,
                  //             child: Column(
                  //                 children: [
                  //                   ClipRRect(
                  //                       borderRadius: BorderRadius.circular(5.0),
                  //                       child: Image(
                  //                         image: NetworkImage('https://i1.sndcdn.com/avatars-D1nDYRK1rfqje9DH-T1eqng-t200x200.jpg'),)
                  //                   ),
                  //                   SizedBox(height: 10,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('fishy',
                  //                       style: TextStyle(
                  //                           color: Colors.white,
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   ),
                  //                   SizedBox(height: 5  ,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('Top Tracks',
                  //                       style: TextStyle(
                  //                           color: Color(0xFF999999),
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   )
                  //                 ]),
                  //           ),
                  //         ],
                  //       ),
                  //       SizedBox(
                  //         width: 15.0,
                  //       ),
                  //       Column(
                  //         children: [
                  //           Container(
                  //             height: 200,
                  //             width: MediaQuery.of(context).size.width * 0.3,
                  //             child: Column(
                  //                 children: [
                  //                   ClipRRect(
                  //                       borderRadius: BorderRadius.circular(5.0),
                  //                       child: Image(
                  //                         image: NetworkImage('https://i1.sndcdn.com/avatars-jssxfQBVrG1qRxl3-q552gA-t200x200.jpg'),)
                  //                   ),
                  //                   SizedBox(height: 10,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('bánh mì pơ tỏi',
                  //                       style: TextStyle(
                  //                           color: Colors.white,
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   ),
                  //                   SizedBox(height: 5  ,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('Top Tracks',
                  //                       style: TextStyle(
                  //                           color: Color(0xFF999999),
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   )
                  //                 ]),
                  //           ),
                  //         ],
                  //       ),
                  //       SizedBox(
                  //         width: 15.0,
                  //       ),
                  //       Column(
                  //         children: [
                  //           Container(
                  //             height: 200,
                  //             width: MediaQuery.of(context).size.width * 0.3,
                  //             child: Column(
                  //                 children: [
                  //                   ClipRRect(
                  //                       borderRadius: BorderRadius.circular(5.0),
                  //                       child: Image(
                  //                         image: NetworkImage('https://i1.sndcdn.com/avatars-qbKTNcAR2B4Hxh5A-iCx6jQ-t200x200.jpg'),)
                  //                   ),
                  //                   SizedBox(height: 10,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('Đặng Gia Linh',
                  //                       style: TextStyle(
                  //                           color: Colors.white,
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   ),
                  //                   SizedBox(height: 5  ,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('Top Tracks',
                  //                       style: TextStyle(
                  //                           color: Color(0xFF999999),
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   )
                  //                 ]),
                  //           ),
                  //         ],
                  //       ),
                  //       SizedBox(
                  //         width: 15.0,
                  //       ),
                  //       Column(
                  //         children: [
                  //           Container(
                  //             height: 200,
                  //             width: MediaQuery.of(context).size.width * 0.3,
                  //             child: Column(
                  //                 children: [
                  //                   ClipRRect(
                  //                       borderRadius: BorderRadius.circular(5.0),
                  //                       child: Image(
                  //                         image: NetworkImage('https://i1.sndcdn.com/avatars-qRlzphJtprKDAUib-2uDGng-t200x200.jpg'),)
                  //                   ),
                  //                   SizedBox(height: 10,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('Hawys.',
                  //                       style: TextStyle(
                  //                           color: Colors.white,
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   ),
                  //                   SizedBox(height: 5  ,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('Top Tracks',
                  //                       style: TextStyle(
                  //                           color: Color(0xFF999999),
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   )
                  //                 ]),
                  //           ),
                  //         ],
                  //       ),
                  //       SizedBox(
                  //         width: 15.0,
                  //       ),
                  //       Column(
                  //         children: [
                  //           Container(
                  //             height: 200,
                  //             width: MediaQuery.of(context).size.width * 0.3,
                  //             child: Column(
                  //                 children: [
                  //                   ClipRRect(
                  //                       borderRadius: BorderRadius.circular(5.0),
                  //                       child: Image(
                  //                         image: NetworkImage('https://i1.sndcdn.com/avatars-B4KwswX95RSOk9AF-DRZAmA-t200x200.jpg'),)
                  //                   ),
                  //                   SizedBox(height: 10,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text("Gii",
                  //                       style: TextStyle(
                  //                           color: Colors.white,
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   ),
                  //                   SizedBox(height: 5  ,),
                  //                   Align(
                  //                     alignment: Alignment.centerLeft,
                  //                     child: Text('Top Tracks',
                  //                       style: TextStyle(
                  //                           color: Color(0xFF999999),
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w500
                  //                       ),),
                  //                   )
                  //                 ]),
                  //           ),
                  //         ],
                  //       ),
                  //       // SizedBox(
                  //       //   width: 15.0,
                  //       // ),
                  //       // Column(
                  //       //   children: [
                  //       //     InkWell(
                  //       //       onDoubleTap: (){
                  //       //         Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailScreen()));
                  //       //       },
                  //       //       child: Container(
                  //       //         height: MediaQuery.of(context).size.height * 0.2,
                  //       //         width: MediaQuery.of(context).size.width * 0.3,
                  //       //         child: Image(image: NetworkImage('https://file.vfo.vn/hinh/2018/03/hinh-anh-hinh-nen-noo-phuoc-thinh-dep-nhat-de-thuong-2.jpg'),
                  //       //
                  //       //
                  //       //         ),
                  //       //       ),
                  //       //     ),
                  //       //
                  //       //     Text('Khong con cua nhau'),
                  //       //     SizedBox(height: 5,),
                  //       //     Text('Ten tac gia')
                  //       //
                  //       //   ],
                  //       // ),
                  //       // SizedBox(
                  //       //   width: 10.0,
                  //       // ),
                  //
                  //     ],
                  //   ),
                  // ),

                  Text('SoundCloud Weekly',
                    style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    'All of SoundCloud. Just for you',
                    style: TextStyle(
                        fontSize: 15,
                        color: Color(0xFF989898),
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(5.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 1,
                        height: MediaQuery.of(context).size.height * 0.55,
                        color: Colors.red,
                        child: Image.network('https://i1.sndcdn.com/artworks-nqe1IzAc0NnLBvjJ-FpXPEw-t200x200.jpg',
                            fit: BoxFit.cover,
                     ),
                      )),

                  SizedBox(height: 5,),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: CircleAvatar(
                          backgroundImage: this._loadImageError?null:NetworkImage('https://lh3.googleusercontent.com/a-/AOh14GgRk0YcMRytT_omkonSktONQd1VsSV0XVQ00Otr6w=s83-c-mo'),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Text('    Based on your listening history', style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF989898),
                        ),),
                      )
                    ],
                  ),
                  SizedBox(height: 30,),
                  Text('Artists You Should Follow',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 28
                    ),
                  ),
                  SizedBox(height: 8,),
                  Text(
                    'Based on your listening history',
                    style: TextStyle(
                        fontSize: 15,
                        color: Color(0xFF989898),
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(height: 10,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: <Widget>[
                        Column(
                          children: [
                            Container(
                              // color: Colors.red,
                              height: 300,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(100.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-LozT9NdP691tpIa3-sjZIPw-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text('TIEN TIEN',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: TextButton(
                                        style: TextButton.styleFrom(
                                          primary: Colors.black,
                                            backgroundColor: Colors.white
                                        ),
                                        onPressed: () { },
                                        child: Text('Follow'),
                                      ),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(100.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-uFtXfyf9FMoR0Mqw-VCAn0A-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text('Mie',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: TextButton(
                                        style: TextButton.styleFrom(
                                            primary: Colors.black,
                                            backgroundColor: Colors.white
                                        ),
                                        onPressed: () { },
                                        child: Text('Follow'),
                                      ),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(100.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-000413802144-x1eus7-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text('nujenQ',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: TextButton(
                                        style: TextButton.styleFrom(
                                            primary: Colors.black,
                                            backgroundColor: Colors.white
                                        ),
                                        onPressed: () { },
                                        child: Text('Follow'),
                                      ),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(100.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-000881860408-vzjn9v-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text('Ngân Hoàng.',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: TextButton(
                                        style: TextButton.styleFrom(
                                            primary: Colors.black,
                                            backgroundColor: Colors.white
                                        ),
                                        onPressed: () { },
                                        child: Text('Follow'),
                                      ),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: Column(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(100.0),
                                        child: Image(
                                          image: NetworkImage('https://i1.sndcdn.com/avatars-ty6ymgdy5uVmECu3-R6QRxg-t200x200.jpg'),)
                                    ),
                                    SizedBox(height: 10,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text("EOVÌ",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500
                                        ),),
                                    ),
                                    SizedBox(height: 5  ,),
                                    Align(
                                      alignment: Alignment.center,
                                      child: TextButton(
                                        style: TextButton.styleFrom(
                                            primary: Colors.black,
                                            backgroundColor: Colors.white
                                        ),
                                        onPressed: () { },
                                        child: Text('Follow'),
                                      ),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                        // SizedBox(
                        //   width: 15.0,
                        // ),
                        // Column(
                        //   children: [
                        //     InkWell(
                        //       onDoubleTap: (){
                        //         Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailScreen()));
                        //       },
                        //       child: Container(
                        //         height: MediaQuery.of(context).size.height * 0.2,
                        //         width: MediaQuery.of(context).size.width * 0.3,
                        //         child: Image(image: NetworkImage('https://file.vfo.vn/hinh/2018/03/hinh-anh-hinh-nen-noo-phuoc-thinh-dep-nhat-de-thuong-2.jpg'),
                        //
                        //
                        //         ),
                        //       ),
                        //     ),
                        //
                        //     Text('Khong con cua nhau'),
                        //     SizedBox(height: 5,),
                        //     Text('Ten tac gia')
                        //
                        //   ],
                        // ),
                        // SizedBox(
                        //   width: 10.0,
                        // ),

                      ],
                    ),
                  ),
                ],
              ),),
          )


        ],
      ),

    );



  }
}
import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mysoundcloud/account_info.dart';
import 'package:mysoundcloud/home_screen.dart';
import 'package:mysoundcloud/library_screen.dart';
import 'package:mysoundcloud/search_screen.dart';
import 'package:mysoundcloud/stream_screen.dart';

import 'detail_screen.dart';


class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _page = 0;
   onPageChanged(int page){
    setState(() {
      _page = page;
    });
  }
  List<Widget> pages = [
    HomeScreen(),
    StreamScreen(),
    SearchScreen(),
    LibraryScreen(),
    //DetailScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: pages[_page],
      bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          backgroundColor: Color(0xFF1e1e1e),
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.grey,
          onTap: onPageChanged,
          currentIndex: _page,
          type: BottomNavigationBarType.fixed,
          unselectedFontSize: 20,
        
        items: 
        const [
          BottomNavigationBarItem(
            icon: Icon(
                Icons.home,
                size: 35,

            ),
            label: "h"
          ),

          BottomNavigationBarItem(
            icon: Icon(
              Icons.bolt,
              size: 35,
            ),
            label: "h"
          ),

          BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
                size: 35,
              ),
              label: "h"
          ),

          BottomNavigationBarItem(
              icon: Icon(
                Icons.library_music,
                size: 35,
              ),
              label: "h"
          ),

          

          
        ]),

    );
  }
}
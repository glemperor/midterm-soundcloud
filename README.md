Soundcloud UI 

HOW TO RUN!
- Step 1: Open the project with Android Studio (Cannot run by command "flutter run")
- Step 2: Go to Device Manager and click on drop down arrow then choose Wipe Data
- Step 3: Open the Android Studio emulator and wait for it connect with Android Studio then press the Run button to run project

FUNCTION:
- Create account by email
- Login with registered account
- Logout 

Provided account:
- Username: teacher@gmail.com
- Password: 123456

Thanks for reading! Have a nice day!